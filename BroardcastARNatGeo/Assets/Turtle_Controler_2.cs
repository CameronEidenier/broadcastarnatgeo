﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Vuforia;

public class Turtle_Controler_2 : MonoBehaviour {
    public GameObject point0;
    public GameObject point1;
    public GameObject point2;
    public GameObject point3;
    public GameObject point4;
    public GameObject point5;
    public GameObject point6;
    public GameObject point7;
    bool ready = true;
    Vector3 previous = new Vector3();
    // Use this for initialization
    void Start()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        CreateTweenSequence();

    }

    // Update is called once per frame
    void Update()
    {
        print(ready);
    }
    public void CreateTweenSequence()
    {
        Vector3[] path = new Vector3[] { point1.transform.localPosition, point2.transform.localPosition, point3.transform.localPosition,
            point4.transform.localPosition, point5.transform.localPosition,point6.transform.localPosition,point7.transform.localPosition,
             point0.transform.localPosition};
        transform.DOLocalPath(path, 30f, PathType.CatmullRom, PathMode.Full3D, 10, Color.green).SetLookAt(.05f).SetEase(Ease.Linear).OnComplete(pathfinnished);
    }
    void pathfinnished()
    {
        print("Ended Loop");
        Destroy(gameObject);
    }
    public void triggerTween()
    {
        //if (ready) CreateTweenSequence();
    }
}
