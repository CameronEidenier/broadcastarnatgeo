﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class CardboardSettingsHandler : MonoBehaviour {

	private bool mCardboardSettingsDialogWasLaunched = false;

	void Start() {
		Cardboard.SDK.OnSettingsDialogLaunched += delegate() {
			mCardboardSettingsDialogWasLaunched = true;
		};
	}

	void Update () {
		if (mCardboardSettingsDialogWasLaunched) {
			mCardboardSettingsDialogWasLaunched = false;

			// Reset Vuforia
			if (VuforiaBehaviour.Instance.enabled) {
				VuforiaBehaviour.Instance.enabled = false;
				VuforiaBehaviour.Instance.enabled = true;
			}
		}
	}
}